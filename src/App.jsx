import React from 'react';
import PersonCard from './components/PersonCard';

const people = [
  {
    name: 'Spongebob Squarepants',
    role: 'Head Chef',
    email: 'spongebob@blah.com',
    phone: '123-456-780',
    avatarImageURL:
      'https://pyxis.nymag.com/v1/imgs/7aa/21a/c1de2c521f1519c6933fcf0d08e0a26fef-27-spongebob-squarepants.2x.rhorizontal.w700.jpg',
  },
  {
    name: 'Patrick Star',
    role: 'Professor of Laying Around',
    email: 'patrick.star@blah.com',
    phone: '123-456-781',
    avatarImageURL:
      'https://i.kym-cdn.com/entries/icons/facebook/000/009/803/spongebob-squarepants-patrick-spongebob-patrick-star-background-225039.jpg',
  },
  {
    name: 'Sandy Cheeks',
    role: 'Head of Sheriffing',
    email: 'sandy.cheeks@blah.com',
    phone: '123-456-782',
    avatarImageURL:
      'https://cdn.costumewall.com/wp-content/uploads/2018/08/sandy-cheeks.jpg',
  },
];

function App() {
  return (
    <div class='w-screen h-screen flex items-center justify-center bg-gray-300 '>
      <div class='flex flex-wrap justify-center'>
        {people.map((person, i) => (
          <div class='m-2'>
            <PersonCard
              name={person.name}
              role={person.role}
              email={person.email}
              phone={person.phone}
              avatarImageURL={person.avatarImageURL}
            />
          </div>
        ))}
      </div>
    </div>
  );
}

export default App;
