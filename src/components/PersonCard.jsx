import React from 'react';

const PersonCard = ({ name, role, email, phone, avatarImageURL }) => {
  return (
    <>
      <div
        class='bg-gray-800 text-white rounded-lg shadow-lg p-6 flex flex-col md:flex-row hover:bg-orange-600 transition ease-in-out duration-500'
        style={{ width: '450px' }}
      >
        <img
          class='h-16 md:h-24 w-16 md:w-24 md:my-auto rounded-full mx-auto md:mx-px md:ml-2 border-yellow-400 border-4 shadow-xl'
          src={avatarImageURL}
        />
        <div class='text-center md:text-left md:ml-2 pt-5 px-5 md:p-5'>
          <div class='text-xl font-bold'>{name}</div>
          <div class='uppercase tracking-widest text-xs text-yellow-200 md:mb-2'>
            {role}
          </div>
          <div>{email}</div>
          <div>{phone}</div>
        </div>
      </div>
    </>
  );
};

export default PersonCard;
